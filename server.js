//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin,X-Requested-With, Content-Type, Accept");
  next();
});

var path = require('path');
var requestjson = require('request-json');
var urlmovimientos = "https://api.mlab.com/api/1/databases/avega/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var cliente= requestjson.createClient(urlmovimientos);


app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/',function(req,res){
  res.sendFile(path.join(__dirname,'index.html'));
});

app.get('/v1/movimientos',function(req,res){
  res.sendFile(path.join(__dirname,'movimientos.json'));
});

app.get('/movimientos',function(req,res){
  cliente.get('',function(err,resp,body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  });
});



app.post('/movimientos',function(req,res){
  cliente.post('',req.body,function(err,resp,body){
      res.send(body);
    });
});

app.put('/',function(req,res){
  res.send("Petición Put Recibido");
});

app.delete('/',function(req,res){
  res.send("Petición Delete Recibido");
});
